# Custom apps and services

This section describes how to extend **Alliance Auth** with custom apps and services.

:::{toctree}
:maxdepth: 1

integrating-services
menu-hooks
url-hooks
logging
:::
